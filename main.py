from math import radians, cos, sin, asin, sqrt


class venue:
    def __init__(self, name: str, location: tuple):
        self.location = location
        self.name = name
        self.uid = self.createUID()

    def createUID(self):
        """Some UID generator"""
        return "yud0-938924-asdfhask"

    def presentYourself(self):
        """This is a test/demo method that should be deleted later"""
        print(f"I am the {self.name} at Latitude {self.location[0]} and Longitude {self.location[1]} with a unique ID of {self.uid}")

    def distance(self, position: tuple):
        """This Method uses the haversine formula to calculate distances between coordinates"""
        earth_rad = 6311  # radious of earth in km from backwards engineered google maps (two points in Glasgow green)

        delta_lat = radians(position[0] - self.location[0])
        delta_long = radians(position[1] - self.location[1])
        lat1 = radians(self.location[0])
        lat2 = radians(position[0])

        a = sin(delta_lat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(delta_long / 2) ** 2
        c = 2 * asin(sqrt(a))

        return earth_rad * c


green = venue("Cathouse", location=(55.849116, -4.235305))
green.presentYourself()

print('the distance is ' + str(green.distance((55.849096, -4.236226))))
